let http = require('http');
let port = 8000


//mock items array
let items = [

	{
		name: "Iphone X",
		description: "Phone designed and created by Apple",
		price: 30000
	},
	{
		name: "Horizon Forbidden West",
		description: "Newest game for the PS4 and PS5",
		price: 4000
	},
	{
		name: "Razer Tiamat",
		description: "Headset from Razer",
		price: 3000
	}
];


http.createServer((req, res) => {

	if (req.url === '/items' && req.method === 'GET') {

		res.writeHead(200, {'Content-type':'application/json'});
		res.end(JSON.stringify(items));

	};

	if (req.url === '/items' && req.method === 'POST') {

		let requestBody = '';

		req.on('data', (data) => {requestBody += data})
		req.on('end', () => {
			requestBody = JSON.parse(requestBody);
			items.push(requestBody);

		res.writeHead(200, {'Content-type':'application/json'});
		res.end(JSON.stringify(items));

		});

	};

	//STRETCH GOAL (data name as plain text)
	if (req.url === '/items/findItem' && req.method === 'POST') {

		let findName = '';
		let findItem = '';

		//You must capture client data first
		req.on('data', (data) => {findName += data});

		//This is where you process the data of your client 
		req.on('end', () => {
			findItem = items.find( (items) => items.name.toLowerCase() === findName.toLowerCase());

		res.writeHead(200, {'Content-type':'application/json'});
		res.end(JSON.stringify(findItem));

		});
	};

	//STRETCH GOAL (data name as JSON)
	if (req.url === '/items/findItem' && req.method === 'POST') {

		let requestBody = '';

		req.on('data', (data) => {requestBody += data})
		req.on('end', () => {
			requestBody = JSON.parse(requestBody);
			
			let foundItem = items.find((item)=> {

				/*
					when anonymous function in the find() method is able to return true, the find() method will return the current item being iterated.

					if filter(), returns an array of items that matches our condition, find() returns the FIRST item to match our condition
				*/
				return item.name === requestBody.name;
			})

		res.writeHead(200, {'Content-type':'application/json'});
		res.end(JSON.stringify(foundItem));

		});

	};


}).listen(port);

console.log(`Server is now running at localhost:${port}`);